// Inserting data through our database
db.users.insertMany([
			{
				firstName: "Jane",
				lastName: "Doe",
				age: 21,
				contact: {
				    phone: "87654321",
				    email: "janedoe@gmail.com"
				},
				courses: [ "CSS", "Javascript", "Python" ],
				department: "none"
			},
		    {
		        firstName: "Stephen",
		        lastName: "Hawking",
		        age: 50,
		        contact: {
		            phone: "87654321",
		            email: "stephenhawking@gmail.com"
		        },
		        courses: [ "Python", "React", "PHP" ],
		        department: "none"
		    },
		    {
		        firstName: "Neil",
		        lastName: "Armstrong",
		        age: 82,
		        contact: {
		            phone: "87654321",
		            email: "neilarmstrong@gmail.com"
		        },
		        courses: [ "React", "Laravel", "Sass" ],
		        department: "none"
		    }
		]);

//[Section] Comparison Query Operators

// $gt/$gte operator
/*
	- Allows us to find documents that have field number values greater than or equal to a specified value.
	- Syntax
		db.collectionName.find({ field : { $gt : value } });
		db.collectionName.find({ field : { $gte : value } });
*/

db.users.find({ age : { $gt : 50 } });
db.users.find({ age : { $gte : 50 } });


// $lt/$lte operator
/*
	- Allows us to find documents that have field number values less than or equal to a specified value.
	- Syntax
		db.collectionName.find({ field : { $lt : value } });
		db.collectionName.find({ field : { $lte : value } });
*/
db.users.find({ age : { $lt : 50 } });
db.users.find({ age : { $lte : 50 } });
// db.users.updateOne({firstName: "Stephen"}, {$set:{age: 50}})

// $ne operator
/*
	- Allows us to find documents that have field number values not equal to a specified value.
	- Syntax
		db.collectionName.find({ field : { $ne : value } });
*/

db.users.find({ age : { $ne: 82 } });

// $in operator
/*
	- Allows us to find documents with specific match criteria one field using different values.
	- Syntax
		db.collectionName.find({ field : { $in : value } });
*/
db.users.find( { lastName: { $in: [ "Hawking", "Doe" ] } } );
db.users.find( { courses: { $in: [ "HTML", "React" ] } } );

// db.users.find({
//     contact: {
//         phone: "87654321",
//         email: "stephenhawking@gmail.com"
//     }
// });

// Query on nested field

db.users.find(
    {"contact.email": "janedoe@gmail.com"}
).pretty();

// [Section] Logical Query Operators

// $or operator
/*
	- Allows us to find documents that match a single criteria from multiple provided search criteria.
	- Syntax
		db.collectionName.find({ $or: [ { fieldA: valueB }, { fieldB: valueB } ] });
*/
multiple field value pairs
db.users.find( { $or: [ { firstName: "Neil" }, { age: "25" } ] } )

db.users.find( { $or: [ { firstName: "Neil" }, { age: { $gt: 30 } } ] } )

// $and operator
/*
	- Allows us to find documents matching multiple criteria in a single field.
	- Syntax
		db.collectionName.find({ $and: [ { fieldA: valueB }, { fieldB: valueB } ] });
*/
db.users.find({ $and: [ { age : { $ne: 82 } }, { age : { $ne: 50 } }] });

// [Section] Field Projection
/*
	- Retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response.
	- When dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.
	- To help with readability of the values returned, we can include/exclude fields from the response.
*/

// Inclusion
/*
	- Allows us to include/add specific fields only when retrieving documents.
	- The value provided is 1 to denote that the field is being included.
	- Syntax
		db.users.find({criteria},{field: 1})
*/
db.users.find( 
	{ 
		firstName: "Jane" 
	}, 
	{ 
		firstName: 1, 
		lastName: 1,
		contact: 1 
	}
);

// Exclusion
/*
	- Allows us to exclude/remove specific fields only when retrieving documents.
	- The value provided is 0 to denote that the field is being included.
	- Syntax
		db.users.find({criteria},{field: 1})
*/
db.users.find( 
	{ 
		firstName: "Jane" 
	}, 
	{ 
		contact: 0, 
		department: 0 
	} 
);

// Suppressing the ID field
/*
	- Allows us to exclude the "_id" field when retrieving documents.
	- When using field projection, field inclusion and exclusion may not be used at the same time.
	- Excluding the "_id" field is the only exception to this rule.
	- Syntax
		db.users.find({criteria},{_id: 0})
*/
db.users.find( 
	{ 
		firstName: "Jane" 
	}, 
	{ 
		firstName: 1, 
		lastName: 1,
		contact: 1,
		_id: 0
	}
);

// Returning Specific Fields in Embedded Documents
db.users.find( 
	{ 
		firstName: "Jane" 
	}, 
	{ 
		firstName: 1, 
		lastName: 1,
		"contact.phone": 1
	}
);

// Supressing Specific Fields in embedded documents
db.users.find( 
	{ 
		firstName: "Jane" 
	}, 
	{ 
		"contact.phone": 0
	}
);

// DOnt discuss this

// Project Specific Array Elements in the Returned Array
// The $slice operator allows us to retrieve only 1 element that matches the search criteria.
db.users.insert({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
});

db.users.find({
    namearr: 
        {
            namea: "juan"
        }
}).pretty();


db.users.find(
	{ "namearr": 
		{ 
			namea: "juan" 
		} 
	}, 
	{ namearr: 
		{ $slice: 1 } 
	}
);

// [Section] Evaluation Query Operators

// $regex operator
/*
	- Allows us to find documents that match a specific string pattern using regular expressions.
	-$options: '$i' - is for Case insensitivity to match upper and lower cases.

	-$options: '$x' - "Extended" capability to ignore all white space characters in the 
	$regex
 	pattern unless escaped or included in a character class.

	// https://www.mongodb.com/docs/manual/reference/operator/query/regex/
	
	- Syntax
		db.users.find({ field: $regex: 'pattern', $options: '$optionValue' });
*/

// Case sensitive query
db.users.find({ firstName: { $regex: 'N' } });

// Case insensitive query
db.users.find({ firstName: { $regex: 'j', $options: '$i' } });

// [SECTION] FIELD PROJECTION
/*
	-Retrieving documents are common operators that we do and by default MongoDB queries return the whole document as a response

	-when dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.
	-To help with readability od the values returned, we can include/exclude fields from the response.
*/

// INCLUSION
/*
	-Allows us to include/add specific fields only when retrieving documents.
	-the value provided is 1 or true to denote that the field is being included
	-Syntax:
		db.users.find({criteria}, {field: 1})
*/

db.users.find(
{
	firstName: "Jane"
},
{
	firstName: true,
	lastName: true,
	contact: true
}
)

// EXCLUSION
/*
	-Allows us to exclude/remove specific fields only when retrieving documents.
	-the value provided is 0 or false to denote that the field is being included
	-Syntax:
		db.users.find({criteria}, {field: 1})
*/

db.users.find(
	{
		firstName: "Stephen"
	},
	{
		contact: 0,
		department: 0
	}
)

// Instances - object inside object
db.users.find(
	{
		firstName: "Stephen"
	},
	{
            "contact.phone": 1,
            _id: 0
	}
);

// [SECTION] Evaluation Query Operators

// $regex operator
/*
	-Allows us to find documents that match a specific string pattern using regular expressions.
	- $options: '$i' - is for Case insensitivity to match upper and lower cases.

	-$options: '$x' - "Extended" capability to ignore all white space characters in the $regex pattern unless escaped or included in a character class.
*/

// Case sensitive query
db.users.find({firstName: {$regex: 'N'}});


// Case insensitive query
db.users.find({firstName: { $regex: 'j', $options: '$i'}})